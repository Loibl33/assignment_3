# Assignment-3: Movies
This application implements a use case of Hibernate and Jpa
## Configuration:
You are welcome to clone the repo!
The application is built on Gradle  v.7.2 with Java v. 17.2.
If you want to run the application locally, you have 2 options.
- 1
    - First you need to run the docker-compose.yml using 
        ```
      docker-compose up -d --no-deps --build
      ```
      This will make an image on postgresDB and the webservices.
    - You must set your preferences in src>main>resources>application.properties and then simply start the Main method.
    - Note that we are using a .env file with our variables like POSTGRES_DB,POSTGRES_PASSWORD and more. This file is not uploaded to this repo because of security reasons. 
- 2
    - You can build a Docker image and use it to run the application.
      <br>
### Usage
You can see and the test REST functionality using Swagger at https://movies-production-app.herokuapp.com/swagger-ui/index.html or check the specifications.
<br>
Please note that https://movies-production-app.herokuapp.com/ is not accessible because the path "/" is not configured. 
<details>
<summary markdown="span">here</summary>

![](SwaggerUI.jpg)
</details>

You can make API calls on https://movies-production-app.herokuapp.com using the following routes:
```
    - actor:
      - POST:   /api/actors/create:                Creates new actor. Actor needs to be specified in request body in json format
      - GET:    /api/actor/get_all:                Returns all actors
      - GET:    /api/actor/{id}:                   Returns an actor specified by id (int)
      - PUT:    /api/actor/update/{id}:            Updates and actor specified by id (int) with actor in request body (You only need to set the propoerties you want to change)
      - DELETE: /api/actor/delete/{id}:            Deletes an actor specified by id (int) 
    - franchise:
      - PUT:    /api/franchise/{id}/update/movies: Update movies of a franchise specified by franchise id and movie ids
      - PUT:    /api/franchise/update/{id}:        Update properties of franchise specified by id (You only need to set the propoerties you want to change)
      - POST:   /api/franchise/create:             Create new franchise
      - GET:    /api/franchise/{id}:               Get franchise by id
      - GET:    /api/franchise/{id}/movies:        Get all movies belonging to a franchise specified by id
      - GET:    /api/franchise/{id}/actors:        Get all actors belonging to a franchise specified by id
      - GET:    /api/franchise/get_all:            Returns all franchises  
      - DELETE: /api/franchise/delete/{id}:        Deletes a franchise specified by id (int)
    - movie:
      - PUT:    /api/movie/{id}/update/actors     Update actors belonging to a movie specified by id
      - PUT:    /api/movie/update/{id}            Update properties from movie specified by id (You only need to set the propoerties you want to change)
      - POST:   /api/movie/create                 Create new movie
      - GET:    /api/movie/{id}                   Get movie by id
      - GET:    /api/movie/{id}/actors            Get actors belonging to a movie specified by movie id
      - GET:    /api/movie/get_all                Get all movies
      - DELETE: /api/movie/delete/{id}            Delete a movie specified by id
```

    
# Security
You can enable security features by uncommenting relating dependencies in build.gradle and code in ```src>main>java>pvd_phil>movies_assignment>config>SecurityConfig.java``` and ```src>main>java>pvd_phil>movies_assignment>config>SecurityConfig.java```



## Maintainer
[Petar Dimitrov]

[Philipp Loibl]
## License
[MIT]
---
[Petar Dimitrov]: https://github.com/PetarDimitrov91
[Philipp Loibl]: https://github.com/Loibl33
[MIT]: https://choosealicense.com/licenses/mit/





