package pvd_phil.movies_assignment.models.dto_models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column
    private String title;
    @Column
    private String genre;
    @Column
    private String releaseYear;
    @Column
    private String director;
    @Column
    private String picture;
    @Column
    private String trailer;
    @Column
    private Long franchiseId;

    @ManyToMany(mappedBy = "movies")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Actor> actors;


    public Movie(String title, String genre, String releaseYear, String director, String picture, String trailer, Long franchiseId) {
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.picture = picture;
        this.trailer = trailer;
        this.franchiseId = franchiseId;
        this.actors = new ArrayList<>();
    }

    public Movie(String title, String genre, String releaseYear, String director, String picture, String trailer, Long franchiseId, List<Actor> actors) {
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.picture = picture;
        this.trailer = trailer;
        this.franchiseId = franchiseId;
        this.actors = actors;
    }
    public Movie() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public Long getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(Long franchiseId) {
        this.franchiseId = franchiseId;
    }

    @JsonGetter()
    public List<Actor> getActors(){
        return actors;
    }

    public void setActors(List<Actor> actors){
        this.actors = actors;
    }


}
