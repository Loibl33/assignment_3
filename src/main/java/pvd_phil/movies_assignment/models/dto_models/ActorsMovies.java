package pvd_phil.movies_assignment.models.dto_models;

import javax.persistence.*;

@Entity
public class ActorsMovies {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private long actorId, movieId;

    public ActorsMovies(long actorId, long movieId) {
        this.actorId = actorId;
        this.movieId = movieId;
    }

    public ActorsMovies() {

    }

    public long getActorId() {
        return actorId;
    }

    public void setActorId(long actorId) {
        this.actorId = actorId;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
