package pvd_phil.movies_assignment.models.dto_models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Actor {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column
    private String fullName;
    @Column
    private String alias;
    @Column
    private Gender gender;
    @Column
    private String picture;

    @ManyToMany
    @JoinTable(name = "actor_movies")
    private List<Movie> movies;

    public Actor(Long id, String fullName, String alias, Gender gender, String picture, List<Movie> movies) {
        this.id = id;
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
        this.movies = movies;
    }

    public Actor(Long id, String fullName, String alias, Gender gender, String picture) {
        this.id = id;
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
        this.movies = new ArrayList<>();
    }

    public Actor() {
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public List<Long> getMovieIds(){
        return this.movies.stream().map((Movie::getId)).toList();
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
