
package pvd_phil.movies_assignment.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

public class CommonResponse<T> {
    public record ErrorDetails(String error, String message) {

        public String getMessage() {
            return message;
        }

        public String getError() {
            return error;
        }
    }

    private  HttpStatus status;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private  T payload;
    private  boolean success;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private  ErrorDetails err;

    public CommonResponse(HttpStatus status, T data) {
        this.status = status;
        this.payload = data;
        this.success = true;
        this.err = null;
    }

    public CommonResponse(HttpStatus status, String err, String message) {
        this.status = status;
        this.payload = null;
        this.success = false;
        this.err = new ErrorDetails(err, message);
    }

    public CommonResponse(HttpStatus status) {
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public T getPayload() {
        return payload;
    }

    public boolean isSuccess() {
        return success;
    }

    public ErrorDetails getErr() {
        return err;
    }
}
