package pvd_phil.movies_assignment.services;

import pvd_phil.movies_assignment.models.dto_models.Actor;
import pvd_phil.movies_assignment.models.dto_models.ActorsMovies;
import pvd_phil.movies_assignment.models.dto_models.Franchise;
import pvd_phil.movies_assignment.models.dto_models.Movie;
import pvd_phil.movies_assignment.repositories.ActorMoviesRepository;
import pvd_phil.movies_assignment.repositories.ActorRepository;
import pvd_phil.movies_assignment.repositories.FranchiseRepository;
import pvd_phil.movies_assignment.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public record Services(MovieRepository movieRepository,
                       FranchiseRepository franchiseRepository,
                       ActorRepository actorRepository,
                       ActorMoviesRepository actorMoviesRepository) {

    public List<Movie> getMoviesFromFranchise(Franchise franchise) {
        return movieRepository.findMovieByFranchiseId(franchise.getId());
    }

    public List<Actor> getActorsFromFranchise(Franchise franchise) {
        Long franchiseId = franchise.getId();
        List<Movie> movies = getMoviesFromFranchise(franchise);

        List<Long> actorIds = movies.stream()
                .map((movie -> actorMoviesRepository.getAllByMovieId(movie.getId())))
                .flatMap(Collection::stream)
                .map(ActorsMovies::getActorId)
                .toList();

        return actorRepository.findAllById(actorIds);
    }

    public List<Actor> getActorsFromMovie(Movie movie) {
        Long movieId = movie.getId();

        List<Long> actorIds = actorMoviesRepository.getAllByMovieId(movieId)
                .stream()
                .map(ActorsMovies::getActorId)
                .toList();

        return actorRepository.findAllById(actorIds);
    }

    public List<Movie> updateMoviesInFranchise(Long[] movieIds, Franchise franchise) {

        Long franchiseId = franchise.getId();
        List<Movie> movies = movieRepository.findAllById(Arrays.asList(movieIds));

        movies.forEach(m -> m.setFranchiseId(franchiseId));

        return movieRepository.saveAll(movies);
    }

    public Movie updateActorsInMovie(Long[] actorIds, Movie movie) {
        List<Actor> newActors = actorRepository.findAllById(Arrays.asList(actorIds));
        Long movieId = movie.getId();

        List<ActorsMovies> movieActors = actorMoviesRepository.getAllByMovieId(movieId);
        actorMoviesRepository.deleteAll(movieActors);

        List<ActorsMovies> newMovieActors = Arrays.stream(actorIds)
                .map(actorId -> new ActorsMovies(actorId, movieId))
                .toList();

        actorMoviesRepository.saveAll(newMovieActors);
        movie.setActors(newActors);

        return movieRepository.save(movie);
    }

    public void deleteActor(Long id) {
        List<ActorsMovies> actorMovRel = actorMoviesRepository.getAllByActorId(id);
        actorMoviesRepository.deleteAll(actorMovRel);

        actorRepository.deleteById(id);
    }

    public void deleteMovie(Long id) {
        List<ActorsMovies> actorMovRel = actorMoviesRepository.getAllByMovieId(id);

        actorMoviesRepository.deleteAll(actorMovRel);
        movieRepository.deleteById(id);
    }

    public void deleteFranchise(Long id) {
        List<Movie> actorMovRel = movieRepository.findMovieByFranchiseId(id).stream()
                .peek(movie -> movie.setFranchiseId(null))
                .toList();

        movieRepository.saveAll(actorMovRel);
        franchiseRepository.deleteById(id);
    }

    public Actor updateActor(Long id, Actor actor) {
        Optional<Actor> actorOpt = actorRepository.findById(id);
        if (actorOpt.isPresent()) {
            Actor actorRec = actorOpt.get();

            actorRec.setFullName(actor.getFullName() != null ? actor.getFullName() : actorRec.getFullName());

            actorRec.setAlias(actor.getAlias() != null ? actor.getAlias() : actorRec.getAlias());

            actorRec.setGender(actor.getGender() != null ? actor.getGender() : actorRec.getGender());

            actorRec.setPicture(actor.getPicture() != null ? actor.getPicture() : actorRec.getPicture());

            actorRec.setMovies(actor.getMovies() != null ? actor.getMovies() : actorRec.getMovies());

            return actorRec;
        }

        return null;
    }

    public Franchise updateFranchise(Long id, Franchise franchise) {
        Optional<Franchise> actorOpt = franchiseRepository.findById(id);
        if (actorOpt.isPresent()) {
            Franchise franchiseRec = actorOpt.get();

            franchiseRec.setName(franchise.getName() != null ? franchise.getName() : franchiseRec.getName());

            franchiseRec.setDescription(franchise.getDescription() != null ? franchise.getDescription() : franchiseRec.getDescription());

            return franchiseRec;
        }

        return null;
    }

    public Movie updateMovie(Long id, Movie movie) {
        Optional<Movie> movieOpt = movieRepository.findById(id);
        if (movieOpt.isPresent()) {
            Movie movieRec = movieOpt.get();

            movieRec.setTitle(movie.getTitle() != null ? movie.getTitle() : movieRec.getTitle());

            movieRec.setGenre(movie.getGenre() != null ? movie.getGenre() : movieRec.getGenre());

            movieRec.setReleaseYear(movie.getReleaseYear() != null ? movie.getReleaseYear() : movieRec.getReleaseYear());

            movieRec.setDirector(movie.getDirector() != null ? movie.getDirector() : movieRec.getDirector());

            movieRec.setPicture(movie.getPicture() != null ? movie.getPicture() : movieRec.getPicture());

            movieRec.setTrailer(movie.getTrailer() != null ? movie.getTrailer() : movieRec.getTrailer());

            movieRec.setFranchiseId(movie.getFranchiseId() != null ? movie.getFranchiseId() : movieRec.getFranchiseId());

            movieRec.setActors(movie.getActors() != null ? movie.getActors() : movieRec.getActors());

            return movieRec;
        }

        return null;
    }
}
