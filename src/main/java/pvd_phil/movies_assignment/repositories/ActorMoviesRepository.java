package pvd_phil.movies_assignment.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pvd_phil.movies_assignment.models.dto_models.ActorsMovies;

import java.util.List;

public interface ActorMoviesRepository extends JpaRepository<ActorsMovies,Long> {
    List<ActorsMovies> getAllByMovieId(Long movieId);
    List<ActorsMovies> getAllByActorId(Long characterId);
}
