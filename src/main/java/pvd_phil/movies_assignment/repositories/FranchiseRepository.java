package pvd_phil.movies_assignment.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pvd_phil.movies_assignment.models.dto_models.Franchise;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
