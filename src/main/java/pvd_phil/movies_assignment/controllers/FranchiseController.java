package pvd_phil.movies_assignment.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pvd_phil.movies_assignment.models.dto_models.Actor;
import pvd_phil.movies_assignment.models.CommonResponse;
import pvd_phil.movies_assignment.models.dto_models.Franchise;
import pvd_phil.movies_assignment.models.dto_models.Movie;
import pvd_phil.movies_assignment.repositories.FranchiseRepository;
import pvd_phil.movies_assignment.services.Services;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class FranchiseController {
    private final FranchiseRepository franchiseRepository;

    private final Services services;

    public FranchiseController(FranchiseRepository franchiseRepository, Services services) {
        this.franchiseRepository = franchiseRepository;
        this.services = services;
    }

    @PostMapping("/franchise/create")
    public ResponseEntity<CommonResponse<Franchise>> createFranchise(@RequestBody Franchise franchise) {
        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.CREATED, franchiseRepository.save(franchise)));
    }

    @GetMapping("/franchise/get_all")
    public ResponseEntity<CommonResponse<Page<Franchise>>> getAllFranchises(@RequestParam("page") String page, @RequestParam("size") String size) {
        Page<Franchise> pageOfFranchises = franchiseRepository.findAll(PageRequest.of(Integer.parseInt(page) - 1, Integer.parseInt(size)));
        if (pageOfFranchises.hasContent()) {
            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK, pageOfFranchises));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Page not found error", "The page was not found!"));
    }

    @GetMapping("/franchise/{id}")
    public ResponseEntity<CommonResponse<Franchise>> getFranchiseById(@PathVariable Long id) {
        if (franchiseRepository.findById(id).isPresent()) {
            return ResponseEntity
                    .ok(new CommonResponse<>(HttpStatus.OK, franchiseRepository.findById(id).get()));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Franchise not found error", "The Franchise was not found!"));
    }

    @GetMapping("/franchise/{id}/movies")
    public ResponseEntity<CommonResponse<List<Movie>>> getMoviesByFranchiseId(@PathVariable Long id) {
        if (franchiseRepository.findById(id).isPresent()) {
            Franchise franchise = franchiseRepository.findById(id).get();
            List<Movie> movies = services.getMoviesFromFranchise(franchise);

            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK,movies));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Movies not found error", "The movies haven't been found!"));
    }

    @GetMapping("/franchise/{id}/actors")
    public ResponseEntity<CommonResponse<List<Actor>>> getActorsByFranchiseId(@PathVariable Long id) {
        if (franchiseRepository.findById(id).isPresent()) {
            Franchise franchise = franchiseRepository.findById(id).get();
            List<Actor> actors = services.getActorsFromFranchise(franchise);

            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK,actors));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Actors not found error", "The actors haven't been found!"));
    }

    @PutMapping("/franchise/update/{id}")
    public ResponseEntity<CommonResponse<Franchise>> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise) {
        Franchise franchiseRec = services.updateFranchise(id, franchise);

        if (franchiseRec != null) {
            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK,franchiseRepository.save(franchiseRec)));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Franchise not modified error", "The franchise was not modified!"));
    }

    @PutMapping("/franchise/{id}/update/movies")
    public ResponseEntity<CommonResponse<List<Movie>>> updateFranchiseMovies(@PathVariable Long id, @RequestBody Long[] movieIds) {
        if (franchiseRepository.findById(id).isPresent()) {
            Franchise franchise = franchiseRepository.findById(id).get();
            List<Movie> movies = services.updateMoviesInFranchise(movieIds, franchise);

            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK, movies));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Franchise-movies not modified error", "The franchise-movies haven't been modified!"));
    }

    @DeleteMapping("/franchise/delete/{id}")
    public ResponseEntity<CommonResponse<Franchise>> deleteFranchiseById(@PathVariable Long id) {
        Optional<Franchise> opt = franchiseRepository.findById(id);

        if (opt.isPresent()) {
            services.deleteFranchise(id);
            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_MODIFIED, "Cannot delete", "The franchise was not deleted!"));
    }
}
