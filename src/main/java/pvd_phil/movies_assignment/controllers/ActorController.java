package pvd_phil.movies_assignment.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pvd_phil.movies_assignment.models.dto_models.Actor;
import pvd_phil.movies_assignment.models.CommonResponse;
import pvd_phil.movies_assignment.repositories.ActorRepository;
import pvd_phil.movies_assignment.services.Services;

import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class ActorController {
    private final ActorRepository actorRepository;
    private final Services services;

    public ActorController(ActorRepository actorRepository, Services services) {
        this.actorRepository = actorRepository;
        this.services = services;
    }

    @PostMapping("/actor/create")
    public ResponseEntity<CommonResponse<Actor>> createActor(@RequestBody Actor actor) {
        return ResponseEntity
                .ok(new CommonResponse<>(HttpStatus.CREATED, actorRepository.save(actor)));
    }

    @GetMapping("/actor/get_all")
    public ResponseEntity<CommonResponse<Page<Actor>>> getAllActors(@RequestParam("page") String page, @RequestParam("size") String size) {

        Page<Actor> actors = actorRepository.findAll(PageRequest.of(Integer.parseInt(page) - 1, Integer.parseInt(size)));

        if (actors.hasContent()) {
            return ResponseEntity
                    .ok(new CommonResponse<>(HttpStatus.OK, actors));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Page not found error", "The was not found!"));
    }

    @GetMapping("/actor/{id}")
    public ResponseEntity<CommonResponse<Actor>> getActorById(@PathVariable Long id) {
        if (actorRepository.findById(id).isPresent()) {
            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK, actorRepository.findById(id).get()));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Actor not found error", "The actor was not found!"));
    }

    @PutMapping("/actor/update/{id}")
    public ResponseEntity<CommonResponse<Actor>> updateActor(@PathVariable Long id, @RequestBody Actor actor) {
        Actor actorRec = services.updateActor(id, actor);

        if (actorRec != null) {
            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK, actorRepository.save(actorRec)));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_MODIFIED, "Actor not modified error", "The actor was not updated!"));
    }

    @DeleteMapping("/actor/delete/{id}")
    public ResponseEntity<CommonResponse<Actor>> deleteActorById(@PathVariable Long id) {
        Optional<Actor> optional = actorRepository.findById(id);

        if (optional.isPresent()) {
            services.deleteActor(id);
            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK));

        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_MODIFIED, "Cannot delete", "The actor was not deleted!"));
    }
}
