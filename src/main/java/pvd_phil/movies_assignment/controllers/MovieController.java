package pvd_phil.movies_assignment.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pvd_phil.movies_assignment.models.dto_models.Actor;
import pvd_phil.movies_assignment.models.CommonResponse;
import pvd_phil.movies_assignment.models.dto_models.Movie;
import pvd_phil.movies_assignment.repositories.MovieRepository;
import pvd_phil.movies_assignment.services.Services;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class MovieController {
    private final MovieRepository movieRepository;
    private final Services services;

    public MovieController(MovieRepository movieRepository, Services services) {
        this.movieRepository = movieRepository;
        this.services = services;
    }

    @PostMapping("/movie/create")
    public ResponseEntity<CommonResponse<Movie>> createMovie(@RequestBody Movie movie) {
        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.CREATED,movieRepository.save(movie)));
    }

    @GetMapping("/movie/get_all")
    public ResponseEntity<CommonResponse<Page<Movie>>> getAllMovies(@RequestParam("page") String page, @RequestParam("size") String size){
        Page<Movie> pageOfMovies = movieRepository.findAll(PageRequest.of(Integer.parseInt(page) - 1, Integer.parseInt(size)));

        if(pageOfMovies.hasContent()){
            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK,pageOfMovies));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Page not found error", "The page was not found!"));
    }

    @GetMapping("/movie/{id}")
    public ResponseEntity<CommonResponse<Movie>> getMovieById(@PathVariable Long id){
        if(movieRepository.findById(id).isPresent()){
            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK, movieRepository.findById(id).get()));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Movie not found error", "The movie was found!"));
    }

    @GetMapping("/movie/{id}/actors")
    public ResponseEntity<CommonResponse<List<Actor>>> getActorsByMovieId(@PathVariable Long id){
        if(movieRepository.findById(id).isPresent()){
            Movie movie = movieRepository.findById(id).get();
            List<Actor> actors = services.getActorsFromMovie(movie);

            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK, actors));
        }

            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Actors not found error", "The actors haven't been found!"));
    }

    @PutMapping("/movie/update/{id}")
    public ResponseEntity<CommonResponse<Movie>> updateMovie(@PathVariable Long id, @RequestBody Movie movie){
        Movie movieRec = services.updateMovie(id,movie);

        if(movieRec != null){
            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK,movieRepository.save(movieRec)));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Movie not modified error", "The movie was not modified!"));
    }

    //TODO -> update works not correct -> FIX
    @PutMapping("/movie/{id}/update/actors")
    public ResponseEntity<CommonResponse<Movie>> updateActorsInMovie(@PathVariable Long id, @RequestBody Long[] movieIds){
        if(movieRepository.findById(id).isPresent()){
            Movie movie = movieRepository.findById(id).get();
            movie = services.updateActorsInMovie(movieIds, movie);

            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK, movie));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_FOUND, "Actors in movies not modified error", "The actors in movies haven't been modified!"));
    }

    @DeleteMapping("/movie/delete/{id}")
    public ResponseEntity<CommonResponse<Movie>> deleteById(@PathVariable Long id){
        Optional<Movie> opt = movieRepository.findById(id);

        if(opt.isPresent()){
            services.deleteMovie(id);
            return ResponseEntity.ok(new CommonResponse<>(HttpStatus.OK));
        }

        return ResponseEntity.ok(new CommonResponse<>(HttpStatus.NOT_MODIFIED, "Cannot delete", "The movie was not deleted!"));
    }
}
